#!/usr/bin/python3

'''This script can be used to download data from aggregator.'''

import AggregatorRESTClient as RESTClient
import json
import datetime
from datetime import datetime,timezone, timedelta
import sys


#Uncomment next line putting your entrypoint
#entrypoint = ""

#Comment next line if you defined entrypoint above
entrypoint = input("Server IP or name: ")

#USE AN ADMIN ACCOUNT 
#username = "your_username"
#password = "your_password"

#or do login with user providee credentials
username = input("User: ")
password = input("Password: ")

aggregator = RESTClient.AggregatorRESTClient(entrypoint,username,password)

def refreshToken(rc):
	rc.login()
	return rc.token

print("Logging in...")
response_status, response_body = aggregator.login()

token = aggregator.token

print("Token: ", aggregator.token)

user_id =  response_body["id"]
user_name = response_body["personalData"]["userName"]

#Use event field to make custom queries
#startTime endTime to query data only on specific intervals
# type and label to get data of a certain device
example_event = {}
example_event["type"] = "TEST"
example_event["label"] = "TEST_LABEL"

# query by example
# WARNING: DO NOT USE FOR LARGE QUERIES -> USE IDBYEXAMPLE INSTEAD
status_code, events = aggregator.getEventsByExample(user_id,example_event)


for event in events["events"]:
	print("----------New event found------------")
	print(event["startTime"]," - ",  event["endTime"])
	print("Type: ", event["type"])
	print("Label: ", event["label"])
	if event["data"] == None:
		print("DATA: No data in payload")
	else:
		print("DATA: ",aggregator.decode_data(event["data"]))

#Get events id by example query
status_code, events = aggregator.getEventsIDsByExample(user_id,example_event)

print(events)

for event in events["eventsID"]:
	print(event["id"])
	