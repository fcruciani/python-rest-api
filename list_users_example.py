#!/usr/bin/python3

'''Exanple script to get list of users.'''

import AggregatorRESTClient as RESTClient
import json
import base64
import datetime
from datetime import datetime,timezone, timedelta
import sys

#Uncomment next line putting your entrypoint
#entrypoint = ""

#Comment next line if you defined entrypoint above
entrypoint = input("Server IP or name: ")

#USE AN ADMIN ACCOUNT 
#username = "your_username"
#password = "your_password"

#or do login with user providee credentials
username = input("User: ")
password = input("Password: ")

aggregator = RESTClient.AggregatorRESTClient(entrypoint,username,password)

def refreshToken(rc):
	rc.login()
	return rc.token

print("Logging in...")
response_status, response_body = aggregator.login()

token = aggregator.token

print("Token: ", aggregator.token)

user_id =  response_body["id"]
user_name = response_body["personalData"]["userName"]

users = aggregator.getUsersList()

for idx,user in enumerate(users):
	user_id = user[0]
	user_name = user[1]
	print("Found Username: "+ str(user[1])+ " - ID:" + str(user[0])  )