#!/usr/bin/python3

'''This script can be used to send data to the aggregator.'''

import AggregatorRESTClient as RESTClient
import json
import datetime
from datetime import datetime,timezone, timedelta
import sys


#Uncomment next line putting your entrypoint
#entrypoint = ""

#Comment next line if you defined entrypoint above
entrypoint = input("Server IP or name: ")

#USE AN ADMIN ACCOUNT 
#username = "your_username"
#password = "your_password"

#or do login with user providee credentials
username = input("User: ")
password = input("Password: ")


aggregator = RESTClient.AggregatorRESTClient(entrypoint,username,password)


def refreshToken(rc):
	rc.login()
	return rc.token

print("Logging in...")
response_status, response_body = aggregator.login()

token = aggregator.token

print("Token: ", aggregator.token)

user_id =  response_body["id"]
user_name = response_body["personalData"]["userName"]

#DATA must be sent inside an event object 
event = {}
#starttime and endtime define time interval the data refers
#Timestamps must be in ISO-8601 format (including timezone)
event["startTime"] = "2019-02-13T10:51:13.777+0100"
event["endTime"] = "2019-02-13T12:51:13.777+0100"
event["type"] = "RAW_DATA"
event["label"] = "FAKE_WIDEFIND"
#for raw data measurement use the data field oin the event
#in this example every event contains two 
event["data"] = aggregator.encode_data("x1,y1,z1,t1\nx2,y2,z2,t2\nx3,y3,z3,t3\n")

# All events must be sent as a list of evevnts
# in case of a single event you add only an event to the list
events = {"events":[]}
events["events"].append(event)

#performing PUSH data query
aggregator.addEvents(user_id,events)